import { ethers, BigNumber } from "ethers";
import { ChainId, Token, WETH, Trade,Fetcher,Route,TokenAmount, TradeType, Percent } from "@pancakeswap/sdk";
import * as fs from 'fs';
import "dotenv/config";


const utf8 = require('utf8');


const provider = new ethers.providers.JsonRpcProvider(process.env.BSC_NODE!);

const privateKey = process.env.PRIVATE_KEY!;

const signer = new ethers.Wallet(privateKey, provider);

const rawRouteAbi = [{"inputs":[{"internalType":"uint256","name":"amountOutMin","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"swapExactETHForTokensSupportingFeeOnTransferTokens","outputs":[],"stateMutability":"payable","type":"function"}]

const rawErc20Abi = [{"inputs":[],"name":"totalFees","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"totalFee","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}]

const routeAbi =rawRouteAbi;
const erc20Abi =rawErc20Abi;


const main = async (contract) => {
    console.log(await provider.getBlockNumber());
    console.log(`The chainId of mainnet is ${ChainId.MAINNET}.`);

    // const tokenToBuyAddress = ethers.utils.getAddress("0xecdbf2fec71ce3007b8fb7b5b46f946d60ba5a05");
    const tokenToBuyAddress = ethers.utils.getAddress(contract);

    
    const bnbAmountToInvest = "0.001";


    const erc20Contract = new ethers.Contract(tokenToBuyAddress, erc20Abi, provider);

    let totalFees, finalBuyTax, slippageTolerance;

    try{
        totalFees = await erc20Contract.totalFees();
        finalBuyTax = totalFees.add(BigNumber.from(2));
        console.log("final buy tax is: %s", finalBuyTax);
    } catch {

    }
    
    try{
        totalFees = await erc20Contract.totalFee();
        finalBuyTax = totalFees.add(BigNumber.from(2));
        console.log("final buy tax is: %s", finalBuyTax);
    } catch {

    }

    if(finalBuyTax > 10) {
        throw "tax too high"
    }

    const tokenToBuy = await Fetcher.fetchTokenData(ChainId.MAINNET, ethers.utils.getAddress(tokenToBuyAddress), provider);

    const BUSD = new Token(
        tokenToBuy.chainId,
        tokenToBuy.address,
        tokenToBuy.decimals
        );


    console.log(BUSD);


    const pair = await Fetcher.fetchPairData(BUSD, WETH[BUSD.chainId], provider);

    // console.log(pair); 

    const route = new Route([pair], WETH[BUSD.chainId]);

    console.log("price is: %s", route.midPrice.toSignificant(8));

    const amountIn = ethers.utils.parseEther(bnbAmountToInvest).toString();

    const trade = new Trade(
        route,
        new TokenAmount(WETH[BUSD.chainId], amountIn),
        TradeType.EXACT_INPUT
      );

    
    // const slippageTolerance = new Percent("10", "10000"); 
    if(finalBuyTax){
        slippageTolerance = new Percent(finalBuyTax.mul(BigNumber.from(100)), "10000");
    } else {
        slippageTolerance = new Percent("100", "10000");
    }
    

    console.log(slippageTolerance);


    const amountOutMin = trade.minimumAmountOut(slippageTolerance).raw;

    console.log("minimun out is : %s", trade.minimumAmountOut(slippageTolerance).raw.toString());

    // throw "over";

    const path = [WETH[BUSD.chainId].address, BUSD.address];

    const to = signer.address; // should be a checksummed recipient address


    const deadline = Math.floor(Date.now() / 1000) + 60 * 20; // 20 minutes from the current Unix time
    const value = trade.inputAmount.raw.toString(); // // needs to be converted to e.g. hex
    
    const routeContract = new ethers.Contract("0x10ED43C718714eb63d5aA57B78B54704E256024E", routeAbi, signer);


    try{
        await routeContract.callStatic.swapExactETHForTokensSupportingFeeOnTransferTokens(
            amountOutMin.toString(),
            path,
            to,
            deadline,
            {value}
        );   
    } catch(e) {
        // console.log("tx cannot be properly excecuted")
        console.error("", `Things exploded (${e})`);
        throw "tx cannot be properly excecuted"
    }

    console.log("tx can be submitted");

    // return "do not buy";


    const tx = await routeContract.swapExactETHForTokensSupportingFeeOnTransferTokens(
        amountOutMin.toString(),
        path,
        to,
        deadline,
        {value}
    );  
  
    console.log("tx hash is: %s", tx.hash);
    
};
  
// main()
//     .then(() => process.exit(0))
//     .catch((error) => {
//       console.error(error);
//       process.exit(1);
//     });
  
module.exports = main;

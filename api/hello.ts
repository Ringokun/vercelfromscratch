import type { VercelRequest, VercelResponse } from '@vercel/node';

const buyTokens = require('../scripts/buyTokens');

export default async function (req: VercelRequest, res: VercelResponse) {
    console.log("start");
    const { name = 'World' } = req.query;
    res.send(`Hello  ${name}!`);
    await buyTokens("0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3");
    console.log("end");
}
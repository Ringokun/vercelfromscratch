const { ethers } = require("ethers");
const buyTokens = require('../scripts/buyTokens');


export default async (req, res) => {
    const { body } = req;
    if(Object.keys(body).includes("contract") && ethers.utils.isAddress(body['contract'])){
        const contract = body['contract'];
        console.log("legit and the contract is %s",contract);
        await buyTokens(contract)
        res.send("Great!");
    } else {
        console.log("no legit contract");
        res.send("Woops!");
    }
}
